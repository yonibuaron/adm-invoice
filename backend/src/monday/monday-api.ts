import mondaySdk from "monday-sdk-js";
import { ICreateInvoice } from "../type";
import { ColumnValue, CompanyItem, ContactItem, PaymentEntry, Indexer, IndexerWithSubItems, InvoiceItem, ItemFileds, ItemQueryResult, Itemslink, OpportunityItem, Product, UserQueryResult, connectBoards, CreateItemVariables, InvoiceEntry, CreateItemMutationResult, UpdateItemVariables } from './type'
import { Stripe } from 'stripe'
import dateFormat from 'dateformat';

export class MondayApi {

  private monday = mondaySdk();
  constructor(apiKey: string) {
    this.monday.setToken(apiKey);
  }

  async boardDetails() {
    const response = await this.monday.api(`
    query {
      boards(ids:3821040977, limit:10) {
        name
        columns {
          id
          title
          type
      }
      items (limit:50){
        id
        name
        column_values {
          id
          value
          text
          title
        }
      }
    }
  }`)

    console.debug(JSON.stringify(response));
  }

  async boards() {
    const response = await this.monday.api(`
    query {
      boards(limit:100) {
        id
        name
  }
}`)

    console.debug(JSON.stringify(response))
  }

  async fetchInvoiceData(opportunityId: string) {
    try {

      // const invoiceItem = await this.mondayQueryItem<InvoiceItem>(invoiceId);
      // if (!invoiceItem) {
      //   throw new Error("Failed to fetch Invoice Item");
      // }

      const opportunityItem = await this.mondayQueryItemWithSubItem<OpportunityItem, Product>(opportunityId);
      if (!opportunityItem) {
        throw new Error("Failed to fetch opportunityItem Item")
      }

      if (opportunityItem.subItems.length == 0) {
        throw new Error("Missing Products");
      }

      // const companyId = opportunityItem.connect_boards;
      // if (!companyId) {
      //   throw new Error("Missing Company");
      // }

      // const opportunityId = invoiceItem.link_to_sales_pipeline;
      // if (!opportunityId) {
      //   throw new Error("Missing Opportunity");
      // }

      // const companyItem = await this.mondayQueryItem<CompanyItem>(companyId);
      // if (!companyItem) {
      //   throw new Error("Failed to fetch Company Item")
      // }

      const contactId = opportunityItem.deal_contact?.id as string;
      if (!contactId) {
        throw new Error("Missing Contact");
      }

      const contactItem = await this.mondayQueryItem<ContactItem>(contactId);
      if (!contactItem) {
        throw new Error("Failed to fetch Contanct Item")
      }

      const invoiceData = await this.createInvoiceRequest(contactItem, opportunityItem);
      return invoiceData;

    } catch (e) {
      console.error("Falied to create invoce error: ", e)
    }
    return null;
  }

  async createInvoiceRequest(contactItem: ContactItem, opportunityItem: OpportunityItem) {
    const invoiceData: ICreateInvoice = {
      opportunityId: opportunityItem.id!,
      items: [],
      customerEmail: contactItem.contact_email,
      customerId: contactItem.id,
      period: opportunityItem.numeric0 ? Number(opportunityItem.numeric0) : undefined,
      discount: opportunityItem.numeric ? Number(opportunityItem.numeric) : undefined,
      billingDayOfMonth: opportunityItem.numeric7 ? Number(opportunityItem.numeric7) : undefined,
      customerName: opportunityItem.text8,
      //dueDate: invoiceItem.date4?.date,
      invoiceDescription: opportunityItem.name,
      currency: opportunityItem.color0,
      address1: contactItem.text9,
      address2: contactItem.text48,
      country: contactItem.text76,
      city: contactItem.text5,
      state: contactItem.text3,
      zip: contactItem.text2,
    }

    for (let product of opportunityItem.subItems) {
      invoiceData.items?.push({
        itemId: product.id!,
        itemDescription: product.name || '',
        amount: Number(product.numeric0),
        discount: product.numeric4 ? Number(product.numeric4) : 0,
        quantity: product.numeric01 ? Number(product.numeric01) : 1
      })
    }

    if (opportunityItem.deal_owner?.personsAndTeams[0]?.id) {
      const response = await this.mondayQueryUser(opportunityItem.deal_owner.personsAndTeams[0].id);

      if (response?.users?.length == 1) {
        invoiceData.agent = response.users[0].name;
      }
    }

    if (!opportunityItem.color) {
      invoiceData.invoiceType = 'invoice';
    } else {
      invoiceData.invoiceType = 'subscription';

      if (opportunityItem.color == 'Monthly') {
        invoiceData.billingCycle = 'month'
      } else if (opportunityItem.color == 'Yearly') {
        invoiceData.billingCycle = 'year';
      } else if (opportunityItem.color == 'Quaterly') {
        invoiceData.billingCycle = 'month';
        invoiceData.billingInterval = 3;
      }
    }

    return invoiceData;
  }

  async createInvoiceEntry(invoice: Stripe.Invoice, subscription?: Stripe.Subscription) {

    const metadata = subscription ? subscription.metadata : invoice.metadata;
    if (metadata == null || !metadata.opportunityId) {
      return null;
    }

    const opportunityItem = await this.mondayQueryItemWithSubItem<OpportunityItem, Product>(metadata.opportunityId);

    const invoiceEntry: InvoiceEntry = {
      text9: invoice.subscription,
      text8: invoice.payment_intent,
      text6: invoice.hosted_invoice_url,
      link_to_sales_pipeline: {
        linkedPulseIds: [{
          linkedPulseId: Number(metadata.opportunityId)
        }]
      },
      collection_status: { index: 1 },
    }

    if (opportunityItem?.board_relation9?.id) {
      invoiceEntry.connect_boards = {
        linkedPulseIds: [{
          linkedPulseId: Number(opportunityItem?.board_relation9?.id)
        }]
      }
    }

    if (invoice.due_date) {
      invoiceEntry.date4 = {
        date: dateFormat(this.fromUnixDate(invoice.due_date), "yyyy-mm-dd"),
      }
    }

    const variables: CreateItemVariables = ({
      boardId: 3770000207,
      itemName: opportunityItem?.name || "",
      columnValues: JSON.stringify(invoiceEntry)
    });

    return await this.mondayCreateItem(variables);
  }

  async createPaymentEntry(invoice: Stripe.Invoice, mondayInvoiceId: string) {

    const paymentItem: PaymentEntry = {
      numeric: invoice.total / 100,
      status: { index: 0 },
      long_text: invoice.payment_intent as string,
      date4: {
        date: dateFormat(new Date(), "yyyy-mm-dd"),
      },
      board_relation0: {
        linkedPulseIds: [{
          linkedPulseId: Number(mondayInvoiceId)
        }]
      }
    };

    const variables: CreateItemVariables = ({
      boardId: 3821040977,
      itemName: invoice.account_name || "",
      columnValues: JSON.stringify(paymentItem)
    });

    return await this.mondayCreateItem(variables);
  }

  async updatePaymentComplated(payment: Stripe.PaymentIntent) {

    const metadata = payment.metadata;
    if (metadata == null || !metadata.mondayPaymentId) {
      return null;
    }

    const updatedColumnValues: any = {
      status: { index: 1 }
    }

    const lastCharge = payment.latest_charge as Stripe.Charge;
    if (lastCharge?.payment_method_details?.card) {
      updatedColumnValues.text9 = lastCharge.payment_method_details.card.last4;
    }

    const variables: UpdateItemVariables = {
      itemId: Number(metadata.mondayPaymentId),
      boardId: 3821040977,
      columnValues: JSON.stringify(updatedColumnValues)
    }

    return await this.mondayUpdateItem(variables);
  }

  async updateInvoiceComplated(invoce: Stripe.Invoice) {

    const metadata = invoce.metadata;
    if (metadata == null || !metadata.mondayInvoiceId) {
      return false;
    }

    const variables: UpdateItemVariables = {
      itemId: Number(metadata.mondayInvoiceId),
      boardId: 3770000207,
      columnValues: JSON.stringify({
        status: { index: 1 }
      })
    }

    return await this.mondayUpdateItem(variables);
  }

  async updatePaymentFailed(payment: Stripe.PaymentIntent) {

    const metadata = payment.metadata;
    if (metadata == null || !metadata.mondayPaymentId) {
      return false;
    }

    const updatedColumnValues: any = {
      status: { index: 2 }
    }

    if (payment.last_payment_error) {
      updatedColumnValues.text1 = payment.last_payment_error.code
      updatedColumnValues.text6 = payment.last_payment_error.message
    }

    const variables: UpdateItemVariables = {
      itemId: Number(metadata.mondayPaymentId),
      boardId: 3821040977,
      columnValues: JSON.stringify(updatedColumnValues)
    }

    return await this.mondayUpdateItem(variables);
  }

  async updatePaymentRefunded(payment: Stripe.PaymentIntent) {
    const metadata = payment.metadata;
    if (metadata == null || !metadata.mondayPaymentId) {
      return false;
    }

    const updatedColumnValues: any = {
      status: { index: 3 }
    }

    const variables: UpdateItemVariables = {
      itemId: Number(metadata.mondayPaymentId),
      boardId: 3821040977,
      columnValues: JSON.stringify(updatedColumnValues)
    }

    return await this.mondayUpdateItem(variables);
  }

  async mondayCreateItem(variables: CreateItemVariables) {
    const query = `mutation create_item ($boardId: Int!, $itemName: String!, $columnValues: JSON!) { 
      create_item (
          board_id: $boardId,
          item_name: $itemName, 
          column_values: $columnValues
      ) 
      { 
          id
      } 
  }`;

    const response = await this.monday.api(query, { variables });

    if (response.data) {
      console.debug("create item response:", response)
      const result = response.data as CreateItemMutationResult;
      return result.create_item.id;
    }

    return null;
  }

  async mondayUpdateItem(variables: UpdateItemVariables) {
    const query = `mutation change_multiple_column_values ($boardId: Int!, $itemId: Int!, $columnValues: JSON!) { 
      change_multiple_column_values (
          board_id: $boardId,
          item_id: $itemId, 
          column_values: $columnValues
      ) 
      { 
          id
      } 
  }`;

    const response = await this.monday.api(query, { variables });

    console.debug("update item response:", response)
    if (response.data) {
      return true;
    }

    return false;
  }

  async mondayQueryItem<T extends Indexer>(itemId: string) {
    console.debug("ItemId: ", itemId)
    const response = await this.monday.api(`
    query {
      items (ids: [${itemId}]) {
        id
        name
        column_values {
          id
          value
          text
          title
          type
        }
      }
  }`)

    if (response.data) {
      //success
      console.debug(JSON.stringify(response.data));

      const result = response.data as ItemQueryResult;
      if (result.items.length > 0) {
        return this.parseItemColumnsToObject<T>(result.items[0]);
      }

      return null;

    } else {
      //@ts-ignore
      throw new Error(JSON.stringify(response.errors))
    }
  }

  async mondayQueryItemWithSubItem<T extends IndexerWithSubItems, TSubItem extends Indexer>(itemId: string) {
    const response = await this.monday.api(`
    query {
      items (ids: [${itemId}]) {
        id
        name
        column_values {
          id
          value
          text
          title
          type
        }
        subitems {
          id
          name
          column_values {
            id
            value
            text
            title
            type
          }
        }
      }
  }`)

    if (response.data) {
      //success
      console.debug(JSON.stringify(response.data));

      const result = response.data as ItemQueryResult;
      if (result.items.length > 0) {
        const itemResult = result.items[0];
        const item = this.parseItemColumnsToObject<T>(itemResult);
        item.subItems = [];

        if (itemResult.subitems) {
          for (let subItem of itemResult.subitems)
            item.subItems.push(this.parseItemColumnsToObject<TSubItem>(subItem))
        }

        return item;
      }

      return null;

    } else {
      //@ts-ignore
      throw new Error(response.error)
    }
  }

  async mondayQueryUser(userId: string) {
    const response = await this.monday.api(`
    query {
      users (ids: [${userId}]) {
        name
        email
      }
  }`);

    if (response.data) {
      console.debug(JSON.stringify(response.data));
      return response.data as UserQueryResult;

    } else {
      //@ts-ignore
      throw new Error(response.error)
    }
  }

  parseItemColumnsToObject<T extends Indexer>(item: ItemFileds) {

    const columnOject: T = {} as T;

    columnOject.id = item.id;
    columnOject.name = item.name
    for (let column of item.column_values) {
      //@ts-ignore
      columnOject[column.id] = this.convertType(column);
    }
    console.debug("columnsObject: ", columnOject)
    return columnOject;
  }

  convertType(column: ColumnValue): any {
    if (column.type == 'color') {
      return column.text;
    }

    if (column.type == 'board-relation') {
      if (column.value) {

        const value = JSON.parse(column.value) as Itemslink;
        if (value?.linkedPulseIds?.length == 1) {
          return {
            id: value.linkedPulseIds[0].linkedPulseId,
            text: column.text
          } as connectBoards
        } else if (value?.linkedPulseIds?.length > 1) {
          return {
            id: value.linkedPulseIds.map(l => l.linkedPulseId),
            text: column.text
          } as connectBoards
        }
      }
      return null;
    }

    if (column.type == 'email') {
      return column.text;
    }

    if (typeof column.value == 'string') {
      return this.tryConvertStringToJSON(column.value);
    }
    return column.value;
  }

  tryConvertStringToJSON(value: any) {
    try {
      return JSON.parse(value);
    } catch {
      return value;
    }
  }

  private fromUnixDate(timestamp: number) {
    return new Date(timestamp * 1000);
  }
}

