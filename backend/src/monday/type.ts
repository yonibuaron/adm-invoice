import { Currency } from "../type"

export type ColumnType = "numeric" | "formula" | "color" | "dropdown" | "date" | "email" | 'board-relation'
export type BillingCycle = "Yearly" | "Quaterly" | "Monthly"

export interface ColumnValue {
  id: string,
  value: any,
  title: string,
  text: string,
  type: ColumnType,
}

export interface ItemFileds {
  id: string,
  name: string,
  column_values: ColumnValue[]
}

export interface Item extends ItemFileds {
  subitems: ItemFileds[]
}

export interface ItemQueryResult {
  items: Item[],
}

export interface UserQueryResult {
  users: User[]
}

export interface CreateItemMutationResult {
  create_item: {
    id: string
  }
}

export interface Indexer {
  [id: string]: any;
  id?: string,
  name?: string;
}

export interface IndexerWithSubItems extends Indexer {
  subItems: Indexer[]
}

export interface Itemslink {
  linkedPulseIds: [{
    linkedPulseId: string
  }]
}

export interface connectBoards {
  id: string | string[],
  text: string
}

export interface CompanyItem extends Indexer {
  account_contact?: connectBoards
}

export interface ContactItem extends Indexer {
  //contact_company: string,
  contact_phone: {
    phone: string,
  }
  contact_email: string,
  location: {
    address: string,
    placeId: string,
  },
  text9: string,//Billing Address 1
  text48: string,//Billing Address 2
  text76: string, //Billing Country
  text5: string,//Billing City
  text3: string,//Billing State
  text2: string,//Billing Zip
  //text6:
}

export interface InvoiceItem extends Indexer {
  connect_boards?: connectBoards,
  //link_to_sales_pipeline?: string, //Opportunity
  date4?: { //expected_collection_date
    date: string
  }
}

export interface OpportunityItem extends IndexerWithSubItems {
  deal_owner: { //agent
    personsAndTeams: [{
      id: string
    }]
  }
  connect_boards: connectBoards,//Rep Name
  color: BillingCycle, // Quaterly, Yearly, Monthly or null
  color0: Currency,
  numeric0: string; // Period
  deal_contact: connectBoards, // Contact 
  board_relation9: connectBoards,
  //connect_boards31: Itemslink, //??
  numeric: string, //discount
  numeric7: string // Subscription Billing Date(Day of the month)
  email: string,
  text8: string, // Compnay Name
  subItems: Product[]

}

export interface Product extends Indexer {
  numeric0: string; //Price
  numeric01?: string //Quantity
  numeric4?: string //Discount
  color: string //Product Name 
}

export interface User extends Indexer {
  email: string,
}

export interface PaymentEntry extends Indexer {
  numeric: number, //Amount
  date4: {
    date: string
  },
  long_text: string,//Payment Intenat
  board_relation0?: { // Monday invoce id
    linkedPulseIds: [{
      linkedPulseId: number
    }]
  }
}

export interface InvoiceEntry extends Indexer {
}

export interface CreateItemVariables {
  boardId: number;
  itemName: string;
  columnValues: string;
}

export interface UpdateItemVariables {
  itemId: number;
  boardId: number;
  columnValues: string;
}