
export interface ICreateInvoice {
  opportunityId: string,
  currency?: Currency,
  invoiceType?: InvoceTypes,
  billingCycle?: BillingCycle,
  billingInterval?: number,
  billingDayOfMonth?: number,
  period?: number,
  discount?: number,
  dueDate?: string,
  agent?: string,
  invoiceDescription?: string,
  items: CreateInvoiceItem[],
  customerId?: string,
  customerName?: string,
  customerEmail?: string,
  address1?: string,
  address2?: string,
  city?: string,
  state?: string,
  country?: string,
  zip?: string,
}

export type Currency = "eur" | "usd";

export type BillingCycle = "day" | "week" | "month" | "year";

export type InvoceTypes = "invoice" | "subscription";

export interface CreateInvoiceItem {
  itemId: string,
  itemDescription: string,
  amount: number,
  discount?: number,
  quantity: number,
}

export interface StripteInvoice {

}