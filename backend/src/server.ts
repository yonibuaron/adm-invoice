

import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import { StripeApi } from './stripe/stripe-api'
import { ICreateInvoice } from './type';
import { MondayApi } from './monday/monday-api';
import Stripe from 'stripe';
//import bodyParser from "body-parser";

const isProd = process.env.NODE_ENV === 'production';

const consoleDebug = console.debug;
console.debug = () => {
  if (isProd) {
    return;
  }
  const now = new Date();
  //@ts-ignore
  consoleDebug.apply(console, [` [debug] [ ${now.getHours()}:${now.getMinutes()} ]`].concat([].slice.call(arguments)))
}

dotenv.config();
const port = process.env.PORT;
const router = express.Router();
const app: Express = express();

app.use(express.json());

// add router in express app
app.use("/", router);

router.use((req: Request, res: Response, next: any) => {
  req.body.stripeApiKey = req.header('STRIPE-API-KEY');
  req.body.mondayApiKey = req.header('MONDAY-API-KEY');

  console.log(req.path);
  next();
})

//opportunityId
router.post('/stripe/create-invoice', async (req: Request, res: Response) => {
  const stripeApi = new StripeApi(req.body.stripeApiKey);
  const mondayApi = new MondayApi(req.body.mondayApiKey);
  const reqData: any = JSON.parse(req.body.data);

  if (!reqData.opportunityId) {
    console.error("Monday opprtunity id is missing");
    return res.status(400).send("Monday opprtunity id is missing");
  }

  const invoiceData: ICreateInvoice | null = await mondayApi.fetchInvoiceData(reqData.opportunityId);

  if (!invoiceData) {
    console.error("Faild to create invoice");
    return res.status(500).send("Faild to create invoice");
  }

  const invoice = await stripeApi.createInvoice(invoiceData);
  if (!invoice) {
    console.error("Faild to create invoice");
    return res.status(500).send("Faild to create invoice");
  }

  console.log("Stripe invoice created successfully, id: ", invoice.id);
  res.end(JSON.stringify({ stripeInvoiceId: invoice.id }));
});

//invoiceId
router.post('/monday/create-invoice', async (req: Request, res: Response) => {
  const stripeApi = new StripeApi(req.body.stripeApiKey);
  const mondayApi = new MondayApi(req.body.mondayApiKey);

  const reqData: any = JSON.parse(req.body.data);

  if (!reqData.invoiceId) {
    console.error("Stripe invoice ID is missing");
    return res.status(400).send("Stripe invoice ID is missing");
  }

  const invoice = await stripeApi.stripe.invoices.retrieve(reqData.invoiceId)
  if (!invoice) {
    console.error("Failed to fetch Stripe invoice");
    return res.status(500).send("Failed to fetch Stripe invoice");
  }

  let subscription: Stripe.Subscription | undefined = undefined
  if (invoice?.subscription) {
    subscription = await stripeApi.stripe.subscriptions.retrieve(invoice.subscription as string);
  }

  const mondayInvoiceId = await mondayApi.createInvoiceEntry(invoice!, subscription);
  if (!mondayInvoiceId) {
    console.error("Failed to create Invoice in monday");
    return res.status(500).send("Failed to create Invoice in monday");
  }

  const mondayPaymentId = await mondayApi.createPaymentEntry(invoice, mondayInvoiceId);
  if (!mondayPaymentId) {
    console.error("Failed to create Payment in monday");
    return res.status(500).send("Failed to create Payment in monday");
  }

  try {
    await stripeApi.stripe.paymentIntents.update(invoice?.payment_intent as string, {
      metadata: {
        mondayPaymentId,
      }
    });

    await stripeApi.stripe.invoices.update(invoice?.id, {
      metadata: {
        ...invoice?.metadata,
        mondayInvoiceId,
      }
    });
  } catch (err) {
    console.error("Failed to update stripe invoice or payment, err:", err)
    return res.status(500).send("Failed to update stripe invoice or payment");
  }

  console.log(`Monday invoice and payment created successfully, invoiceId: ${mondayInvoiceId}, paymentId: ${mondayPaymentId}`);
  res.end(JSON.stringify({ mondayPaymentId, mondayInvoiceId }));
})

//paymentIntent
router.post('/monday/payment-completed', async (req: Request, res: Response) => {
  const stripeApi = new StripeApi(req.body.stripeApiKey);
  const mondayApi = new MondayApi(req.body.mondayApiKey);
  const reqData: any = JSON.parse(req.body.data);

  if (!reqData.paymentIntent) {
    console.error("Stripe payment intent is missing");
    return res.status(400).send("Stripe payment intent is missing");
  }

  try {
    const payment = await stripeApi.stripe.paymentIntents.retrieve(reqData.paymentIntent, {
      expand: ["latest_charge", "invoice"]
    });

    if (!payment) {
      console.error("Failed to fetch Stripe payment intent");
      return res.status(500).send("Failed to fetch Stripe payment intent");
    }

    const success = await mondayApi.updatePaymentComplated(payment);
    if (success) {
      await mondayApi.updateInvoiceComplated(payment.invoice as Stripe.Invoice);
    }

    console.log("Monday update payment success: ", success);
    res.end(JSON.stringify({
      success,
    }));

  } catch (err) {
    console.error("Failed to fetch stripe payment, err:", err)
    return res.status(500).send("Failed to fetch stripe payment");
  }
})

//paymentIntent
router.post('/monday/payment-failed', async (req: Request, res: Response) => {
  const stripeApi = new StripeApi(req.body.stripeApiKey);
  const mondayApi = new MondayApi(req.body.mondayApiKey);
  const reqData: any = JSON.parse(req.body.data);

  if (!reqData.paymentIntent) {
    console.error("Stripe payment intent is missing");
    return res.status(400).send("Stripe payment intent is missing");
  }

  try {
    const payment = await stripeApi.stripe.paymentIntents.retrieve(reqData.paymentIntent);
    if (!payment) {
      console.error("Failed to fetch Stripe payment intent");
      return res.status(500).send("Failed to fetch Stripe payment intent");
    }

    const success = await mondayApi.updatePaymentFailed(payment);
    console.log("Monday update payment success: ", success);

    res.end(JSON.stringify({
      success,
    }));
  } catch (err) {
    console.error("Failed to fetch stripe payment, err:", err)
    return res.status(500).send("Failed to fetch stripe payment");
  }
})

//paymentIntent
router.post('/monday/payment-refunded', async (req: Request, res: Response) => {
  const stripeApi = new StripeApi(req.body.stripeApiKey);
  const mondayApi = new MondayApi(req.body.mondayApiKey);
  const reqData: any = JSON.parse(req.body.data);

  if (!reqData.paymentIntent) {
    console.log("Stripe payment intent is missing");
    return res.status(400).send("Stripe payment intent is missing");
  }

  try {
    const payment = await stripeApi.stripe.paymentIntents.retrieve(reqData.paymentIntent);
    if (!payment) {
      console.error("Failed to fetch Stripe payment intent");
      return res.status(500).send("Failed to fetch Stripe payment intent");
    }

    const success = await mondayApi.updatePaymentRefunded(payment);
    console.log("Monday update payment success: ", success);

    res.end(JSON.stringify({
      success,
    }));
  } catch (err) {
    console.error("Failed to fetch stripe payment, err:", err)
    return res.status(500).send("Failed to fetch stripe payment");
  }
})

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});