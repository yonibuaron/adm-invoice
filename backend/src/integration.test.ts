import { MondayApi } from './monday/monday-api';
import { ICreateInvoice } from './type';
import { StripeApi } from './stripe/stripe-api'
import Stripe from 'stripe';

//const SPRITE_API_KEY = "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";
const SPRITE_API_KEY = "sk_test_51JtzmsLK7U4nn0Rx5ZgetGJynNAEsQCb6ld53tdXsqdZIUAXtMISEcggmOemcCCnajHNtjVTBYUN41S2QRzjmQm600LbhO3tzz";
const stripeApi = new StripeApi(SPRITE_API_KEY)

const MONDAY_API_KEY = "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjIwMzEzOTk4NSwidWlkIjozMjg3OTUxNCwiaWFkIjoiMjAyMi0xMi0wMVQxNTo0OTo1Ny4wMDBaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6MTI5NTU2NTksInJnbiI6InVzZTEifQ.ULdNRHnCkyiTbGKHgdVJhVOLdE4hLr0c-nFIkPbhB70";
const mondayApi = new MondayApi(MONDAY_API_KEY)

describe('Create invoice flow', () => {
  test('fetch data from monday and create invoice in stripe and save the result in payment monday', async () => {

    const mondayOpportunityId = "3770000510";
    const invoiceData: ICreateInvoice | null = await mondayApi.fetchInvoiceData(mondayOpportunityId);

    if (!invoiceData) {
      return fail("Invoice id is null");
    }

    let invoice = await stripeApi.createInvoice(invoiceData!);

    if (!invoice) {
      return fail("Invoice is null");
    }

    let subscription: Stripe.Subscription | undefined = undefined

    if (invoice?.subscription) {
      subscription = await stripeApi.stripe.subscriptions.retrieve(invoice.subscription as string);
    }

    const mondayInvoiceId = await mondayApi.createInvoiceEntry(invoice!, subscription);
    if (!mondayInvoiceId) {
      return fail("Failed to create invoice in monday");
    }

    const mondayPaymentId = await mondayApi.createPaymentEntry(invoice, mondayInvoiceId);
    if (!mondayPaymentId) {
      return fail("Failed to create Payment in monday");
    }

    let payment = await stripeApi.stripe.paymentIntents.update(invoice?.payment_intent as string, {
      metadata: {
        mondayPaymentId,
      }
    });

    payment = await stripeApi.stripe.paymentIntents.retrieve(payment.id, {
      expand: ["latest_charge", "invoice"]
    });

    invoice = await stripeApi.stripe.invoices.update(invoice?.id, {
      metadata: {
        ...invoice?.metadata,
        mondayInvoiceId,
      }
    });

    await mondayApi.updatePaymentComplated(payment);
    await mondayApi.updateInvoiceComplated(payment.invoice as Stripe.Invoice);

    expect(true).toBe(true);

  }, 1000 * 100);

});
