import { ICreateInvoice } from '../type';
import { StripeApi } from './stripe-api'

const SPRITE_API_KEY = "sk_test_51MYXEbDMGgDnRugNCAbhX3SEdalILlmK72iL4jybOUUZsBt78srIo8lxV57vKm9QVrY1pMxMSJ3JDWrNmk102nU9005ArV6SHR";
//const SPRITE_API_KEY = "sk_test_51JtzmsLK7U4nn0Rx5ZgetGJynNAEsQCb6ld53tdXsqdZIUAXtMISEcggmOemcCCnajHNtjVTBYUN41S2QRzjmQm600LbhO3tzz";
const stripeApi = new StripeApi(SPRITE_API_KEY)

describe('createInvoice', () => {

  beforeAll(async () => {
    //await stripeApi.cleanInvoices();
  })

  test('create a one time invoce', async () => {

    const invoiceData: ICreateInvoice = {
      opportunityId: '3770000510',
      invoiceType: 'invoice',
      items: [{
        itemId: "test_item_id_1",
        amount: 350,
        quantity: 2,
        itemDescription: "test_item1",
      }, {
        itemId: "test_item_id_2",
        amount: 150,
        quantity: 3,
        itemDescription: "test_item1",
      }
      ],
      discount: 20,
      dueDate: "2026-01-24",
      currency: "eur",
      agent: "test_agent",
      customerId: "5435345",
      customerName: "test_name",
      customerEmail: "yonibu@gmail.com",
      address1: "dsgsdfg",
      //address2: "Elysia Park",
      //city: "Agoura Hills",
      //state: "CA",
      //country: "USA",
      zip: "91301a",
    }

    const result = await stripeApi.createInvoice(invoiceData);

    expect(result?.id).not.toBeUndefined();

  }, 1000 * 10);

  test('create a subscription invoce', async () => {

    const invoiceData: ICreateInvoice = {
      opportunityId: '3770000510',
      invoiceType: 'subscription',
      invoiceDescription: 'test_subscrition_invoice',
      billingCycle: 'month',
      period: 10,
      items: [{
        itemId: "test_item_id_1",
        amount: 350,
        quantity: 2,
        itemDescription: "test_item1",
      }, {
        itemId: "test_item_id_2",
        amount: 150,
        quantity: 3,
        itemDescription: "test_item1",
      }
      ],
      discount: 25,
      dueDate: "2026-01-24",
      currency: "usd",
      agent: "test_agent",
      customerId: "2333355",
      customerName: "test_name_yoni",
      customerEmail: "yonibu@gmail.com",
      address1: "Sotiraki Markidi 22",
      address2: "Elysia Park",
      city: "Paphos",
      state: "Universal",
      country: "Cyprus",
      zip: "8036",
    }

    const result = await stripeApi.createInvoice(invoiceData);

    expect(result?.id).not.toBeUndefined();

  }, 1000 * 10);
});
