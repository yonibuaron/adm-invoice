import { Stripe } from 'stripe'
import { ICreateInvoice, CreateInvoiceItem } from '../type';

export class StripeApi {

  stripe: Stripe

  constructor(stripeApiKey: string) {
    this.stripe = new Stripe(stripeApiKey, {
      //typescript: true,
      apiVersion: "2022-11-15",
    });
  }

  async cleanInvoices() {
    const invoices = await this.stripe.invoices.list();

    for (let i of invoices.data) {
      await this.stripe.invoices.del(i.id);
    }

    console.debug(invoices);
  }

  async createInvoice(data: ICreateInvoice) {

    try {
      const customer = await this.getOrCreateCustomer(data);

      if (data.invoiceType === 'invoice') {
        return await this.createOneTimeInvoice(data, customer);
      } else if (data.invoiceType === 'subscription') {

        return await this.createSubscriptionInvoice(data, customer);
      }

    } catch (e) {
      console.error(`falied to creeate invoice: ${e}`)
    }
    return null;
  }

  private async createOneTimeInvoice(data: ICreateInvoice, customer: Stripe.Customer) {
    // let dueDate: number = data.dueDate ? Date.parse(data.dueDate) : new Date().getTime() + 60000;

    // if (Date.now() > dueDate) {
    //   dueDate = new Date().getTime() + 60000;
    // }

    const invoiceData: Stripe.InvoiceCreateParams = {
      customer: customer.id,
      days_until_due: 30,
      // due_date: Math.floor(dueDate / 1000), // UNIX timestamp
      description: data.invoiceDescription || '',
      collection_method: 'send_invoice',
      currency: data.currency || "USD",
      metadata: {
        agent: data.agent || '',
        opportunityId: data.opportunityId
      },
    };


    if (data.discount) {
      const discount = Number(data.discount);
      if (discount > 0 && discount < 100) {
        const coupon = await this.stripe.coupons.create({
          name: "Discount",
          percent_off: discount,
        })
        invoiceData.discounts = [{
          coupon: coupon.id
        }]
      }
    }

    let invoice = await this.stripe.invoices.create(invoiceData)

    for (let item of data.items) {
      const discount = item.discount || 0;
      const finalAmount = item.amount * (100 - discount) / 100;

      await this.stripe.invoiceItems.create({
        customer: customer.id,
        description: item.itemDescription,
        unit_amount: finalAmount * 100, // in cents
        quantity: item.quantity,
        invoice: invoice.id,
      })
    }

    invoice = await this.stripe.invoices.finalizeInvoice(invoice.id);

    console.debug(invoice);

    return invoice;
  }

  private async createSubscriptionInvoice(data: ICreateInvoice, customer: Stripe.Customer) {

    const sub: Stripe.SubscriptionCreateParams = {
      customer: customer.id,
      description: data.invoiceDescription || '',
      currency: data.currency || "USD",
      collection_method: 'send_invoice',
      proration_behavior: 'create_prorations',
      days_until_due: 30,
      metadata: {
        agent: data.agent || '',
        opportunityId: data.opportunityId
      },
    };

    const productItems: Stripe.SubscriptionCreateParams.Item[] = [];

    for (let item of data.items) {
      const discount = item.discount || 0;
      const finalAmount = item.amount * (100 - discount) / 100;
      productItems.push({
        quantity: item.quantity,
        price_data: {
          unit_amount: finalAmount * 100, //in cents
          currency: data.currency || "USD",
          product: (await this.getOrCreateProduct(item)).id,
          recurring: {
            interval: data.billingCycle!,
            interval_count: data.billingInterval || 1
          }
        }
      }
      )
    }
    sub.items = productItems;

    if (data.discount && data.discount > 0 && data.discount < 100) {
      const coupon = await this.stripe.coupons.create({
        name: "Discount",
        percent_off: data.discount,
      })
      sub.coupon = coupon.id;
    }

    const now = new Date();
    let anchorDate: Date;
    if (data.billingDayOfMonth) {
      let dateBase = new Date(now);
      if (data.billingDayOfMonth <= now.getUTCDay()) {
        dateBase.setMonth(dateBase.getUTCMonth() + 1)
      }
      anchorDate = new Date(dateBase.getUTCFullYear(), dateBase.getUTCMonth(), data.billingDayOfMonth);
    } else {
      const tomorrow = new Date(now);
      tomorrow.setDate(tomorrow.getUTCDate() + 1);
      anchorDate = tomorrow;
    }

    console.debug(anchorDate.toDateString());
    sub.billing_cycle_anchor = this.unixDate(anchorDate);

    // let startDate = new Date();
    // startDate.setDate(startDate.getUTCDate() - 1);
    // sub.backdate_start_date = this.unixDate(startDate);

    if (data.period) {
      const cancelDate = new Date(anchorDate);
      cancelDate.setMonth(cancelDate.getUTCMonth() + data.period)
      console.debug(cancelDate.toDateString());
      sub.cancel_at = this.unixDate(cancelDate);
    }

    console.debug(JSON.stringify(sub, null, "\t"));

    const subscription = await this.stripe.subscriptions.create(sub)

    console.debug(subscription);

    if (subscription.latest_invoice && typeof subscription.latest_invoice == 'string') {
      let lastInvoice = await this.stripe.invoices.retrieve(subscription.latest_invoice)
      lastInvoice = await this.stripe.invoices.finalizeInvoice(lastInvoice.id);
      console.debug(JSON.stringify(lastInvoice, null, "\t"));
      return lastInvoice;
    }

    return null;
  }

  private async getOrCreateProduct(item: CreateInvoiceItem) {
    try {
      return await this.stripe.products.retrieve(item.itemId);
    } catch (e) {
    }
    return await this.createProduct(item);
  }

  private async createProduct(item: CreateInvoiceItem) {
    return await this.stripe.products.create({
      id: item.itemId,
      name: item.itemDescription,
      //description: item.itemDescription,
    })
  }

  private async getOrCreateCustomer(data: ICreateInvoice) {
    let customer = await this.findCustomer(data);
    if (!customer) {
      customer = await this.stripe.customers.create({
        name: data.customerName || '',
        email: data.customerEmail,
        address: {
          city: data.city,//Paphos
          country: data.country, // cy
          line1: data.address1,
          line2: data.address2,
          postal_code: data.zip,
          state: data.state
        },
        metadata: { mondayId: data.customerId || '' }
      });
    }

    return customer;
  }

  private async findCustomer(data: ICreateInvoice) {

    const serachParam: Stripe.CustomerSearchParams = {
      query: `metadata['mondayId']:'${data.customerId}'`,
    }

    const searchRes = await this.stripe.customers.search(serachParam);
    if (searchRes.data?.length > 0) {
      return searchRes.data[0]
    }

    return null;
  }

  private unixDate(date: number | Date) {
    let timestamp;

    if (typeof date == 'number') {
      timestamp = date;
    } else {
      timestamp = date.getTime();
    }
    return Math.floor(timestamp / 1000);
  }
}