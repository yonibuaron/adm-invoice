"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StripeApi = void 0;
var stripe_1 = require("stripe");
var StripeApi = /** @class */ (function () {
    function StripeApi(stripeApiKey) {
        this.stripe = new stripe_1.Stripe(stripeApiKey, {
            //typescript: true,
            apiVersion: "2022-11-15",
        });
    }
    StripeApi.prototype.getInvoices = function () {
        return __awaiter(this, void 0, void 0, function () {
            var invoices;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.stripe.invoices.list()];
                    case 1:
                        invoices = _a.sent();
                        // for (let i of invoices.data) {
                        //   await this.stripe.invoices.del(i.id);
                        // }
                        console.log(invoices);
                        return [2 /*return*/];
                }
            });
        });
    };
    StripeApi.prototype.createInvoice = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var customer, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.getOrCreateCustomer(data)];
                    case 1:
                        customer = _a.sent();
                        if (data.invoiceType === 'invoice') {
                            return [2 /*return*/, this.createOneTimeInvoice(data, customer)];
                        }
                        else if (data.invoiceType === 'subscription') {
                            return [2 /*return*/, this.createSubscriptionInvoice(data, customer)];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        console.error("falied to creeate invoice: ".concat(e_1));
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/, null];
                }
            });
        });
    };
    StripeApi.prototype.createOneTimeInvoice = function (data, customer) {
        return __awaiter(this, void 0, void 0, function () {
            var dueDate, invoiceData, coupon, invoice, _i, _a, item, invoiceItem;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        dueDate = data.dueDate ? Date.parse(data.dueDate) : new Date().getTime() + 60000;
                        if (Date.now() > dueDate) {
                            dueDate = new Date().getTime() + 60000;
                        }
                        invoiceData = {
                            customer: customer.id,
                            due_date: Math.floor(dueDate / 1000),
                            description: data.invoiceDescription || '',
                            collection_method: 'send_invoice',
                            metadata: { agent: data.agent || '' },
                            currency: data.currency || "USD",
                        };
                        if (!(data.discount && data.discount > 0 && data.discount < 100)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.stripe.coupons.create({
                                name: "Discount",
                                percent_off: data.discount,
                            })];
                    case 1:
                        coupon = _b.sent();
                        invoiceData.discounts = [{
                                coupon: coupon.id
                            }];
                        _b.label = 2;
                    case 2: return [4 /*yield*/, this.stripe.invoices.create(invoiceData)];
                    case 3:
                        invoice = _b.sent();
                        _i = 0, _a = data.items;
                        _b.label = 4;
                    case 4:
                        if (!(_i < _a.length)) return [3 /*break*/, 7];
                        item = _a[_i];
                        return [4 /*yield*/, this.stripe.invoiceItems.create({
                                customer: customer.id,
                                description: item.itemDescription,
                                unit_amount: item.amount * 100,
                                quantity: item.quantity,
                                //tax_code: "txcd_20060055",	//Marketing Services
                                invoice: invoice.id,
                            })];
                    case 5:
                        invoiceItem = _b.sent();
                        _b.label = 6;
                    case 6:
                        _i++;
                        return [3 /*break*/, 4];
                    case 7: return [4 /*yield*/, this.stripe.invoices.finalizeInvoice(invoice.id)];
                    case 8:
                        invoice = _b.sent();
                        // invoice = await this.stripe.invoices.retrieve(invoice.id);
                        console.log(invoice);
                        return [2 /*return*/, invoice];
                }
            });
        });
    };
    StripeApi.prototype.createSubscriptionInvoice = function (data, customer) {
        return __awaiter(this, void 0, void 0, function () {
            var sub, productItems, _i, _a, item, _b, _c, coupon, subscription;
            var _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        sub = {
                            customer: customer.id,
                            description: data.invoiceDescription || '',
                            currency: data.currency || "USD",
                            metadata: { agent: data.agent || '' },
                            collection_method: 'send_invoice',
                            days_until_due: 0,
                        };
                        productItems = [];
                        _i = 0, _a = data.items;
                        _f.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        item = _a[_i];
                        _c = (_b = productItems).push;
                        _d = {
                            quantity: item.quantity
                        };
                        _e = {
                            unit_amount: item.amount * 100,
                            currency: data.currency || "USD"
                        };
                        return [4 /*yield*/, this.getOrCreateProduct(item)];
                    case 2:
                        _c.apply(_b, [(_d.price_data = (_e.product = (_f.sent()).id,
                                _e.recurring = {
                                    interval: 'month',
                                    interval_count: data.period || 1
                                },
                                _e),
                                _d)]);
                        _f.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4:
                        sub.items = productItems;
                        if (!(data.discount && data.discount > 0 && data.discount < 100)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.stripe.coupons.create({
                                name: "Discount",
                                percent_off: data.discount,
                            })];
                    case 5:
                        coupon = _f.sent();
                        sub.coupon = coupon.id;
                        _f.label = 6;
                    case 6: return [4 /*yield*/, this.stripe.subscriptions.create(sub)];
                    case 7:
                        subscription = _f.sent();
                        console.log(subscription);
                        return [2 /*return*/, subscription];
                }
            });
        });
    };
    StripeApi.prototype.getOrCreateProduct = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.stripe.products.retrieve(item.itemId)];
                    case 1: return [2 /*return*/, _a.sent()];
                    case 2:
                        e_2 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [4 /*yield*/, this.createProduct(item)];
                    case 4: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    StripeApi.prototype.createProduct = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.stripe.products.create({
                            id: item.itemId,
                            name: item.itemDescription,
                            //description: item.itemDescription,
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    StripeApi.prototype.getOrCreateCustomer = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var customer;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.findCustomer(data)];
                    case 1:
                        customer = _a.sent();
                        if (!!customer) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.stripe.customers.create({
                                name: data.customerName || '',
                                email: data.customerEmail,
                                address: {
                                    city: data.city,
                                    country: data.country,
                                    line1: data.address1,
                                    line2: data.address2,
                                    postal_code: data.zip,
                                    state: data.state
                                },
                                metadata: { mondayId: data.customerId || '' }
                            })];
                    case 2:
                        customer = _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/, customer];
                }
            });
        });
    };
    StripeApi.prototype.findCustomer = function (data) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var serachParam, searchRes;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        serachParam = {
                            query: "metadata['mondayId']:'".concat(data.customerId, "'"),
                        };
                        return [4 /*yield*/, this.stripe.customers.search(serachParam)];
                    case 1:
                        searchRes = _b.sent();
                        if (((_a = searchRes.data) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                            return [2 /*return*/, searchRes.data[0]];
                        }
                        return [2 /*return*/, null];
                }
            });
        });
    };
    return StripeApi;
}());
exports.StripeApi = StripeApi;
//# sourceMappingURL=stripe-api.js.map