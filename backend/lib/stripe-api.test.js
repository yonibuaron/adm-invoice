"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var stripe_api_1 = require("./stripe-api");
var SPRITE_API_KEY = "sk_test_51JtzmsLK7U4nn0Rx5ZgetGJynNAEsQCb6ld53tdXsqdZIUAXtMISEcggmOemcCCnajHNtjVTBYUN41S2QRzjmQm600LbhO3tzz";
var stripeApi = new stripe_api_1.StripeApi(SPRITE_API_KEY);
describe('createInvoice', function () {
    test('create a one time invoce', function () { return __awaiter(void 0, void 0, void 0, function () {
        var invoiceData, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    invoiceData = {
                        invoiceType: 'invoice',
                        items: [{
                                itemId: "test_item_id_1",
                                amount: 350,
                                quantity: 2,
                                itemDescription: "test_item1",
                            }, {
                                itemId: "test_item_id_2",
                                amount: 150,
                                quantity: 3,
                                itemDescription: "test_item1",
                            }
                        ],
                        discount: 20,
                        dueDate: "2026-01-24",
                        currency: "EUR",
                        agent: "test_agent",
                        customerId: "23333",
                        customerName: "test_name",
                        customerEmail: "yonibu@gmail.com",
                        address1: "Sotiraki Markidi 22",
                        address2: "Elysia Park",
                        city: "Paphos",
                        state: "Universal",
                        country: "Cyprus",
                        zip: "8036",
                    };
                    return [4 /*yield*/, stripeApi.createInvoice(invoiceData)];
                case 1:
                    result = _a.sent();
                    expect(result === null || result === void 0 ? void 0 : result.id).not.toBeUndefined();
                    return [2 /*return*/];
            }
        });
    }); }, 1000 * 10);
    test('create a subscription invoce', function () { return __awaiter(void 0, void 0, void 0, function () {
        var invoiceData, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    invoiceData = {
                        invoiceType: 'subscription',
                        invoiceDescription: 'test_subscrition_invoice',
                        period: 10,
                        items: [{
                                itemId: "test_item_id_1",
                                amount: 350,
                                quantity: 2,
                                itemDescription: "test_item1",
                            }, {
                                itemId: "test_item_id_2",
                                amount: 150,
                                quantity: 3,
                                itemDescription: "test_item1",
                            }
                        ],
                        discount: 25,
                        dueDate: "2026-01-24",
                        currency: "EUR",
                        agent: "test_agent",
                        customerId: "23333",
                        customerName: "test_name",
                        customerEmail: "yonibu@gmail.com",
                        address1: "Sotiraki Markidi 22",
                        address2: "Elysia Park",
                        city: "Paphos",
                        state: "Universal",
                        country: "Cyprus",
                        zip: "8036",
                    };
                    return [4 /*yield*/, stripeApi.createInvoice(invoiceData)];
                case 1:
                    result = _a.sent();
                    expect(result === null || result === void 0 ? void 0 : result.id).not.toBeUndefined();
                    return [2 /*return*/];
            }
        });
    }); }, 1000 * 10);
});
//# sourceMappingURL=stripe-api.test.js.map