"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const perform = async (z, bundle) => {
    const response = await z.request({
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        url: 'https://adm-invoice.onrender.com/create-invoice',
        body: JSON.parse(bundle.inputData.data)
    });
    return response.data;
};
exports.default = {
    key: 'createInvoice',
    noun: 'createInvoice',
    display: {
        label: 'Create Invoice',
        description: 'Create invoice for customer.',
    },
    operation: {
        perform,
        inputFields: [
            { key: 'data', label: "Data (JSON)", type: 'string', required: true },
        ],
        sample: {
            amount: 1000,
            quantity: 2,
        },
    },
};
