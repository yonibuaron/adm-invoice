"use strict";
/* globals describe, expect, test */
Object.defineProperty(exports, "__esModule", { value: true });
const zapier_platform_core_1 = require("zapier-platform-core");
const index_1 = require("../index");
const appTester = zapier_platform_core_1.createAppTester(index_1.default);
zapier_platform_core_1.tools.env.inject();
describe('createInvoice', () => {
    test('create a invoce', async () => {
        const invoiceData = {
            data: JSON.stringify({
                amount: 350,
                quantity: 2,
                currency: "EUR",
                agent: "test_agent",
                itemDescription: "unit test",
                customerId: "23333",
                customerName: "test_name",
                customerEmail: "yonibu@gmail.com",
                address1: "Sotiraki Markidi 22",
                address2: "Elysia Park",
                city: "Paphos",
                state: "Universal",
                country: "Cyprus",
                zip: "8036",
            }),
        };
        const bundle = { inputData: invoiceData };
        const result = await appTester(index_1.default.creates.createInvoice.operation.perform, bundle);
        expect(result === null || result === void 0 ? void 0 : result.id).not.toBeUndefined();
    });
});
