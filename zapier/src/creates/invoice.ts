import { Bundle, ZObject } from 'zapier-platform-core';
import { ICreateInvoice } from '../type';

const perform = async (
  z: ZObject,
  bundle: Bundle<ICreateInvoice>
) => {
  const response = await z.request({
    method: 'POST',
    headers: {
      "Content-Type": "application/json"
    },
    url: 'https://adm-invoice.onrender.com/create-invoice',
    body: JSON.parse(bundle.inputData.data)
  });

  return response.data;
};

export default {
  key: 'createInvoice',
  noun: 'createInvoice',

  display: {
    label: 'Create Invoice',
    description: 'Create invoice for customer.',
  },

  operation: {
    perform,
    inputFields: [
      { key: 'data', label: "Data (JSON)", type: 'string', required: true },
    ],
    sample: {
      amount: 1000,
      quantity: 2,
    },
  },
};
