/* globals describe, expect, test */

import { createAppTester, tools } from 'zapier-platform-core';

import App from '../index';
import { ICreateInvoice } from '../type';

const appTester = createAppTester(App);
tools.env.inject();

describe('createInvoice', () => {
  test('create a invoce', async () => {

    const invoiceData: ICreateInvoice = {
      data: JSON.stringify({
        items: [{
          amount: 350,
          quantity: 2,
          itemDescription: "test_item1",
        }, {
          amount: 150,
          quantity: 3,
          itemDescription: "test_item1",
        }
        ],
        currency: "EUR",
        agent: "test_agent",
        customerId: "23333",
        customerName: "test_name",
        customerEmail: "yonibu@gmail.com",
        address1: "Sotiraki Markidi 22",
        address2: "Elysia Park",
        city: "Paphos",
        state: "Universal",
        country: "Cyprus",
        zip: "8036",
      }),
    }

    const bundle = { inputData: invoiceData };
    const result = await appTester(App.creates.createInvoice.operation.perform, bundle);

    expect(result?.id).not.toBeUndefined();

  });
});
