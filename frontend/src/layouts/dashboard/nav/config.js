// component
import SvgColor from '../../../components/svg-color';

// ----------------------------------------------------------------------

const icon = (name) => <SvgColor src={`/assets/icons/${name}.svg`} sx={{ width: 1, height: 1 }} />;

const navConfig = [
  {
    title: 'Create New Invoice',
    path: '/dashboard/app',
    icon: icon('form'),
  },
  {
    title: 'Invoices',
    path: '/dashboard/user',
    icon: icon('list'),
  },
];

export default navConfig;
